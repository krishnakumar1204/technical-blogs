### What is callback?
Callback is a javascript function that is passed as an argument into another function. We use callbacks if we want to call a function only after completion of one task. 

Javascript is a single-threaded, synchronous language which executes the code sequentially from top to bottom. Callback enables us to do things in JavaScript asynchronously. 

**Example:**
```
console.log("Hello");
```
If we run the above code the "Hello" will be printed instantly after hitting the run button.
But, if we want to print the "Hello" after few seconds(or after completion of something in the code), then we can do this by using callback function.

``` 
setTimeout(function(){
    console.log("Hello");
}, 5000);
```
Here in the code we have wrapped the "console.log("Hello")" inside a function and passed it to the *setTimeout()* function as a callback function. Now if we run this code then the "Hello" will be printed on the console after 5sec.

### Why we need Callback function?
JavaScript is a single-threaded synchronous language. It blocks the execution further code if it takes some time to execute a particular line of code. But by using callback function we can ensure that the execution doesn't get blocked instead it executes the further part of code.

### Advantages of Callback function
Callback is a very powerful and useful concept of JavaScript. There are many advantages of using callback function in JavaScript code. Some of them are:

1. **Handle async tasks:** In javascript if it take some time to fetch some data from an API then the execution of the code will be blocked till the time it won't be able to fetch the data. But by using callback function we can handle this by doing some other task during it is fetching data.
2. **Uses in Event Handling:** Callback functions are also used in event handling as well. If we want to show something to the user if the user presses a button on the screen, then we can use callback function to call the function after the "click" event happend.

### Disadvantages of Callback Function
Callback function is very useful and powerful despite that there are some disadvantages of using callback function that can be very dangerous at times. Here are some disadvantages of using callback function:
1. **Callback Hell:** If we use pass a callback function inside a function, pass that function inside another function and so on using nested function, then the code grows horizontally instead of vertically. It becomes very hard to handle, manage and read this kind of codes. for example:
```
taksOne(param1, () => {
    console.log("Callback function inside taskOne.");

    taskTwo(param2, () => {
        console.log("Callback function inside taskTwo.");

        taskThree(param3, () => {
            console.log("Callback function inside taskThree.");

            taksFour(param4, () => {
                console.log("Callback function inside taskFour.");
                .
                .
                .

            })
        })
    })
})
```
This is a simple example where we can still be able to read, manage the code, but in real code it become very difficult to handle and manage this thing as there are hundreds of lines of code inside a single functions.

2. **Inversion of Control:** When we pass a callback function to function then we loose the control over the callback function and we give the control of the callback function to that function. In this kind of case if there occurs an error inside the main function then the callback function might not be called or might not be called as we were expecting it to be. This can be very dangerous in real-life code if there occurs some error in the main function.
```
api.createOrder(cart, () => {

    api.initiatePayment();
})
```

In the above code if there occur an error in the API call to create order then there can be case the initiate payment API call does differently than expected or it did not get called.